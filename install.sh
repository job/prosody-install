#!/bin/bash
# Deploys Prosody on a new server

source "./config.sh"

echo "================================"
echo "Installing miscellaneous packages to run Prosody"

apt-get update && apt-get upgrade -y
apt-get update && apt-get install -y mercurial socat

echo "================================"
echo "Adding Prosody Repository Key"

echo deb http://packages.prosody.im/debian $(lsb_release -sc) main | tee /etc/apt/sources.list.d/prosody.list
wget https://prosody.im/files/prosody-debian-packages.key -O /etc/apt/trusted.gpg.d/prosody.gpg

echo "================================"
echo "Running apt update"

apt-get update

echo "================================"
echo "Installing Prosody"

apt-get update && apt-get install -y prosody lua5.3 luarocks lua-event lua-sec lua-zlib libicu70 libicu-dev libunbound-dev liblua5.3-dev
luarocks install luaunbound

echo "================================"
echo "Making directories"

mkdir -p /etc/prosody/certs
mkdir -p /etc/prosody/conf.d

echo "================================"
echo "Configuring UFW firewall"

# ufw allow 80
ufw allow 5222
ufw allow 5269
ufw allow 5347
ufw allow 5280
ufw allow 5281

echo "================================"
echo "Pulling configs and modules"

hg clone https://hg.prosody.im/prosody-modules/ "${PROSODY_DATA_DIR}"/modules

echo "================================"
echo "Generating DH key, this is a long procedure"

openssl dhparam -out /etc/prosody/certs/dh-4096.pem 4096

echo "================================"
echo "Reloading prosody configuration"

systemctl reload prosody.service
