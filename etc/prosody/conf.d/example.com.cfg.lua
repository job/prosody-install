VirtualHost "PROSODY_DOMAIN"

enabled = true

anonymous_login = false
allow_registration = false
min_seconds_between_registrations = 300

welcome_message = "Welcome"

ssl = {
 key = "/etc/prosody/certs/PROSODY_DOMAIN.key";
 certificate = "/etc/prosody/certs/PROSODY_DOMAIN.crt";
 options = { "no_sslv2", "no_sslv3", "no_ticket", "no_compression" };
 ciphers = "ECDH:DH:!CAMEL!LIA128:!3DES:MD5:!RC4:!aNULL:!NULL:!EXPORT:!LOW:!MEDIUM";
 dhparam = "/etc/prosody/certs/dh-4096.pem";
}