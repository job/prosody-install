# This script will clean log files and route logging to /dev/null

## Disable syslog

# update-rc.d rsyslog disable
sudo systemctl stop rsyslog.service
sudo systemctl disable rsyslog.service

## Disable journald logging

echo 'Storage=none' >> /etc/systemd/journald.conf

## Clean all log files without deleting them

find /var/log/ -type f -exec cp /dev/null {} \;

## Disable server logging via symlinking to /dev/null

ln -sf /dev/null /var/log/prosody/prosody.err
ln -sf /dev/null /var/log/prosody/prosody.log
ln -sf /dev/null /root/.bash_history
ln -sf /dev/null /root/.viminfo
ln -sf /dev/null /var/log/auth.log
ln -sf /dev/null /var/log/wtmp
ln -sf /dev/null /var/log/messages
ln -sf /dev/null /var/log/lastlog
ln -sf /dev/null /var/log/auth.log
ln -sf /dev/null /var/log/syslog
ln -sf /dev/null /var/log/daemon.log
ln -sf /dev/null /var/log/dpkg.log
ln -sf /dev/null /var/log/faillog
ln -sf /dev/null /var/log/ufw.log
