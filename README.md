# Prosody XMPP

Prosody XMPP server installation

## Other

Prosody main configuration file

```sh
/etc/prosody/prosody.cfg.lua
```

Prosody log storage

```sh
/var/log/prosody
```

Prosody data storage

```sh
/var/lib/prosody
```

How to add a user

```sh
prosodyctl adduser nick@domain
```

Restar a server

```sh
prosodyctl restart
```
