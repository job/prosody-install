#!/bin/bash
# This script ensures that files and directories are chowned properly

source "./config.sh"

# Set permsission and reload service

chown -R prosody:prosody "${PROSODY_ETC}"
chown -R prosody:prosody "${PROSODY_DATA_DIR}"
chmod -R 700 "${PROSODY_CERTS}" # have to check if these are correct permissions @todo
systemctl reload prosody.service

