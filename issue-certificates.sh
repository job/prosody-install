#!/bin/bash
# Deploys Prosody on a new server

source "./config.sh"

echo "================================"
echo "Creating LE certificates and adding renew hooks"


curl https://get.acme.sh | sh

# ZeroSSL, ACME is working with ZeroSSL by default now

# /root/.acme.sh/acme.sh --register-account -m "${ACME_SH_EMAIL}" --server zerossl
# /root/.acme.sh/acme.sh --server zerossl --issue --standalone -d "${PROSODY_DOMAIN}" --fullchain-file /etc/prosody/certs/"${PROSODY_DOMAIN}".crt --key-file /etc/prosody/certs/"${PROSODY_DOMAIN}".key
# /root/.acme.sh/acme.sh --reloadcmd "service prosody restart" --install-cronjob

# Letsencrypt

/root/.acme.sh/acme.sh --server letsencrypt --issue --standalone -d "${PROSODY_DOMAIN}" --fullchain-file /etc/prosody/certs/"${PROSODY_DOMAIN}".crt --key-file /etc/prosody/certs/"${PROSODY_DOMAIN}".key
/root/.acme.sh/acme.sh --reloadcmd "service prosody restart" --install-cronjob

echo "Reloading prosody configuration"

systemctl reload prosody.service
systemctl restart prosody.service
