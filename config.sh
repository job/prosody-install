#!/bin/bash
# Deploys Prosody on a new server

#GIT_DIR="/home/user/git"
export PROSODY_DATA_DIR="/var/lib/prosody"
export PROSODY_CERTS="/etc/prosody/certs"
export PROSODY_ETC="/etc/prosody"
export ACME_SH_DIR="/root/.acme.sh"
export PROSODY_DOMAIN="domain.com"
export PROSODY_CONFERENCE_DOMAIN=""
export ACME_SH_EMAIL=""
